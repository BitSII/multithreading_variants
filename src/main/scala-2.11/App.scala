import java.io.File
import java.util.Properties

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import service.actors.MyHttpServiceActor

import scala.concurrent.duration._
import scala.io.Source

/**
  * Created by root on 11.07.16.
  */
object App {
  def main(args: Array[String]): Unit = {
//    val file = new File("/home/ivan/new-stakan4ik/frontend/test.txt")
//    val path: String = file.ge
// tAbsolutePath
//    val name: String = file.getName
//    val path1: String = file.getPath
//    val path2: String = file.getCanonicalPath
    val PATH = getClass.getResource("css/test.css").getPath
    // we need an ActorSystem to host our application in
    implicit val system = ActorSystem("on-spray-can")

    val service = system.actorOf(Props[MyHttpServiceActor], "async-server")

    implicit val timeout = Timeout(5.seconds)
    IO(Http) ? Http.Bind(service, interface = "localhost", port = 8091)
  }
}
