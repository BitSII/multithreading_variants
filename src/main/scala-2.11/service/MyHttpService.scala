package service

import spray.http.HttpResponse
import spray.routing._


trait MyHttpService extends HttpService {

  def highloadFutureTask(): HttpResponse
  def highloadThreadTask(): HttpResponse

  private val RESOURCES_PATH = "src/main/resources/"

  val myRoute = {
    rejectEmptyResponse {
      (get & path("future")) {
          complete(highloadFutureTask())
      } ~
      (get & path("thread")) {
        complete(highloadThreadTask())
      }
    }
  }
}
