package service.actors

import java.util.concurrent.{CountDownLatch, ExecutorService, Executors}

import akka.actor.Actor
import http.HttpAnswer
import service.MyHttpService
import spray.http.HttpResponse

//import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future, duration}
import scala.io.Source

/**
  * Created by root on 22.11.16.
  */
class MyHttpServiceActor extends Actor with MyHttpService {
  val THREAD_POOL_SIZE = 8
  val FUTURE_POOL_SIZE = 8
//  implicit private val exContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(FUTURE_POOL_SIZE))

  val pool: ExecutorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE)

  private val FRONTEND_PATH = "/frontend/"
  private val PROJECT_DIR = System.getProperty("user.dir") + FRONTEND_PATH

  def actorRefFactory = context
  
  def receive = runRoute(myRoute)

  override def highloadFutureTask(): HttpResponse = {
    val filePath1 = PROJECT_DIR + "1.txt"
    val filePath2 = PROJECT_DIR + "2.txt"
    val filePath3 = PROJECT_DIR + "3.txt"

    val finalFuture1 = executeOnFileFuture(filePath1)
    val finalFuture2 = executeOnFileFuture(filePath2)
    val finalFuture3 = executeOnFileFuture(filePath3)

    val finalFuture = for {
      result1 <- finalFuture1
      result2 <- finalFuture2
      result3 <- finalFuture3
    } yield result1 + result2 + result3

    val result: String = Await.result(finalFuture, Duration(10, duration.SECONDS))

    val httpAnswer = new HttpAnswer()
    httpAnswer.withContentType(".txt")
    httpAnswer.withContent(result)
    httpAnswer.getResponse()
  }

  override def highloadThreadTask(): HttpResponse = {
    var report1: String = ""
    var report2: String = ""
    var report3: String = ""

    val latch: CountDownLatch = new CountDownLatch(3)

    pool.execute(new Runnable {
      override def run() = {
        report1 = executeOnFileThread(PROJECT_DIR + "1.txt")
        latch.countDown()
      }
    })
    pool.execute(new Runnable {
      override def run() = {
        report2 = executeOnFileThread(PROJECT_DIR + "2.txt")
        latch.countDown()
      }
    })
    pool.execute(new Runnable {
      override def run() = {
        report3 = executeOnFileThread(PROJECT_DIR + "3.txt")
        latch.countDown()
      }
    })

    latch.await()

    val httpAnswer = new HttpAnswer()
    httpAnswer.withContentType(".txt")
    httpAnswer.withContent(report1 + report2 + report3)
    httpAnswer.getResponse()
  }


  def nonMemoryTask(intNumbers: List[Int]): Int = {
    var countOdd = 0
    intNumbers.foreach(intNumber => {
      if (intNumber % 2 != 0) {
        countOdd += 1
      }
    })
    countOdd
  }

  def memoryTask(intNumbers: List[Int]): Int = intNumbers.sortWith((a, b) => a < b).last

  def executeOnFileFuture(filePath: String): Future[String] = {
    val fileName = filePath.substring(filePath.lastIndexOf('.') - 1, filePath.length)
    readFileTaskFuture(filePath).flatMap { intNumbers =>
      val memory: Future[Int] = sortNumbersFuture(intNumbers, fileName)
        val nonMemory: Future[Int] = getOddNumbersFuture(intNumbers, fileName)
        for {
          lastElement <- memory
          remainder <- nonMemory
        } yield generateReportFutures(remainder, lastElement)
    }
  }

  def readFileTaskFuture(filePath: String): Future[List[Int]] = {
    val lines = readLines(filePath)
    Future[List[Int]] {
      lines.map(Integer.parseInt).toList
    }
  }

  def getOddNumbersFuture(intNumbers: List[Int], fileName: String): Future[Int] = {
    println(s"Starting execute non memory task of file : $fileName")
    Future[Int] {
      val countOdd = nonMemoryTask(intNumbers)
      println(s"Ending   execute non memory task of file : $fileName")
      countOdd
    }
  }

  def sortNumbersFuture(intNumbers: List[Int], fileName: String): Future[Int] = {
    println(s"Starting execute     memory task of file : $fileName")
    Future[Int] {
      val last = memoryTask(intNumbers)
      println(s"Ending   execute     memory task of file : $fileName")
      last
    }
  }


  def executeOnFileThread(filePath: String): String = {
    val localLatch: CountDownLatch = new CountDownLatch(2)
    var countOddNumbers: Int = 0
    var lastElement: Int = 0

    val fileName = filePath.substring(filePath.lastIndexOf('.') - 1, filePath.length)
    val intNumbers = readFileTaskThread(filePath)

    pool.execute(new Runnable {
      override def run() = {
        countOddNumbers = getOddNumbersThread(intNumbers, fileName)
        localLatch.countDown()
      }
    })
    pool.execute(new Runnable {
      override def run() = {
        lastElement = sortNumbersThread(intNumbers, fileName)
        localLatch.countDown()
      }
    })

    localLatch.await()
    generateReportThreads(countOddNumbers, lastElement)

  }

  def readFileTaskThread(filePath: String): List[Int] = {
    readLines(filePath).map(Integer.parseInt).toList
  }

  def getOddNumbersThread(intNumbers: List[Int], fileName: String): Int = {
    println(s"Thread starting execute non memory task of file : $fileName")
    val countOdd = nonMemoryTask(intNumbers)
    println(s"Thread ending   execute non memory task of file : $fileName")
    countOdd
  }

  def sortNumbersThread(intNumbers: List[Int], fileName: String): Int = {
    println(s"Thread starting execute     memory task of file : $fileName")
    val last = memoryTask(intNumbers)
    println(s"Thread ending   execute     memory task of file : $fileName")

    last
  }

  def generateReportThreads(rem: Int, lastEl: Int): String = {
    val sb = new StringBuilder("Calculating with threads \n")
    sb.append("Count of odd numbers : ").append(rem).append('\n')
    sb.append("The last element in the sorted sequence : ").append(lastEl).append('\n')
    sb.toString()
  }

  def generateReportFutures(rem: Int, lastEl: Int): String = {
    val sb = new StringBuilder("Calculating with futures \n")
    sb.append("Count of odd numbers : ").append(rem).append('\n')
    sb.append("The last element in the sorted sequence : ").append(lastEl).append('\n')
    sb.toString()
  }

  def readLines(filePath: String) = {
    val lines: Iterator[String] = null
    try {
      Source.fromFile(filePath).getLines()
    } catch {
      case e: Throwable =>
        println(e.getMessage)
        lines
    }
  }
}
