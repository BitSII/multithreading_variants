package http

import spray.http.HttpHeaders._
import spray.http.MediaTypes._
import spray.http._

import scala.collection.immutable.HashMap

/**
  * Created by root on 22.11.16.
  */
class HttpAnswer {
  val answer: StringBuilder = new StringBuilder()
  var contentType: MediaType = `text/plain`

  def withContentType(filePath: String): Unit = {
    val extention = filePath.substring(filePath.lastIndexOf('.') + 1, filePath.length)
    println(extention)
    HttpAnswer.extensions.get(extention) match {
      case Some(ct) => contentType = ct
      case None => contentType = `text/plain`
    }
  }

  def withContent(content: String): Unit = {
    answer.append(content)
  }

  def getResponse(): HttpResponse = {
    val bytes = answer.toString.getBytes
    val entity: HttpEntity = HttpEntity(contentType, bytes)
    val resp: HttpResponse = HttpResponse(200, entity)
    resp
  }
}

object HttpAnswer {
  val HTTP_TITLE = "HTTP/1.0 "
  val CODE_OK = "200 OK"

  val extensions: Map[String, MediaType] = new HashMap[String, MediaType]() ++ List(
    "html" -> `text/html`,
    "css" -> `text/css`,
    "js" -> `application/javascript`)
}
